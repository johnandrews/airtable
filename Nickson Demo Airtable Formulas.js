Airtable Formulas

Customers > id
IF({last.name}, 
    CONCATENATE({last.name}, ", ", {first.name}, " - ", {client.type}),
    IF({email},
        CONCATENATE({email}, " - ", {client.type}),
        CONCATENATE({status}, " - ", {client.type})))

        IF({last.name}, 
            CONCATENATE({last.name}, ", ", {first.name}),
            IF({email},
                CONCATENATE({email}),
                CONCATENATE({status})))

Customer > age
IF({birthdate}, 
    CONCATENATE(DATETIME_DIFF(TODAY(), {birthdate}, 'years'), " years old"), 
        IF({client.type} = "B2B", "B2B customer", "we don't know"))

Customers > ListeningStateChangedEvent.time.remaining
IF({move.out},
    IF(DATETIME_DIFF({move.out}, TODAY(), 'days') > 0, 
        IF(DATETIME_DIFF({move.out}, TODAY(), 'days') > 1, CONCATENATE(DATETIME_DIFF({move.out}, TODAY(), 'days'), " days") , " day"), 
            "move out already occurred on " & DATETIME_FORMAT({move.out}, 'MM/DD/YYYY')),
            "Check contract! Update records.") 

Customers > tenant.move.out.to.nickson.move.out
IF({tenant.move.out},
    IF(DATETIME_DIFF({tenant.move.out}, {move.out}, 'days') > 0, 
        CONCATENATE(DATETIME_DIFF({tenant.move.out}, {move.out}, 'days'), " days after Nickson move out"),
        CONCATENATE(DATETIME_DIFF({tenant.move.out}, {move.out}, 'days'), " days before Nickson move out. Reschedule!")),
    "please get customer's tenant move out")

Customers > days.between.account.creation.checkout
    IF({checkout.complete},
        IF(DATETIME_DIFF( {checkout.complete}, {account.creation}, 'days') > 0, 
            IF(DATETIME_DIFF({checkout.complete}, {account.creation}, 'days') > 1, 
                CONCATENATE(DATETIME_DIFF({checkout.complete}, {account.creation}, 'days'), " days"),
                "1 day"), 
                    "same day " & DATETIME_FORMAT({account.creation}, 'MM/DD/YYYY')),
        "booked via phone and/or email") 

Customers > days.between.checkout.move.in
IF({checkout.complete}, 
    IF(DATETIME_DIFF({move.in.date}, {checkout.complete}, 'days') = 1, "1 day from checkout to move in", 
        IF(DATETIME_DIFF({move.in.date}, {checkout.complete}, 'days') > 0, 
            CONCATENATE("customer web checked out ", DATETIME_DIFF({move.in.date}, {checkout.complete}, 'days'), " days before move in"),
            IF({move.in.date} = {checkout.complete}, CONCATENATE("customer checkout and move in are same day ", DATETIME_FORMAT({move.in.date}, 'MM/DD/YYYY')), "ERROR! Move in scheduled before checkout"))),
"B2B client or no checkout date")

move_order > summary
IF({sq.ft},
    CONCATENATE({flow}, " - ", {unit}, " - ", {sq.ft}, " sq ft"),
    CONCATENATE({flow}, " - ", {unit}))

Units > id
    IF({building}, 
        CONCATENATE({building}, " - ", {client.type}, " - ", {home.type}),
        CONCATENATE({client.type}, " - ", {home.type}, " - ", {management.company}))

Units > sales.tax
        IF({state} = "IL", {monthly.rate}* 0.0975,
            IF({state} = "FL", {monthly.rate}* 0.075,
                IF({state} = "DC", {monthly.rate}* 0.0575,
                {monthly.rate}* 0.0825)))

buildings > location
IF({building} = "N/A",
    {full.address},
    CONCATENATE({building}, " - ", {management.company}))

time_tracker > normal_hours
    IF({total_hours} > 40,
            40,
            {total_hours})

time_tracker > overtime_hours
    IF({total_hours} > 40,
        {total_hours} - 40)

time_off > days_off
    IF({time_off_start_date} = BLANK(), 
        " ",
        WORKDAY_DIFF({time_off_start_date}, {time_off_end_date}, '2020-07-03, 2020-5-25, 2020-09-7, 2020-11-26, 2020-12-25'))

time_off > name
    IF({days_off} > 0,
       (((employee & ": ") & DATESTR({time_off_start_date})) & " to ") & DATESTR({time_off_end_date}),
       "No time off scheduled")
Designer Checklist Block

function formatList(items) {
    if (items.length == 1) {
        return items[0];
    }
    return items.slice(0, -1).join(', ') + ' and ' + items[items.length - 1];
}

async function validateFields(tableName, fieldNames) {
    let table = base.getTable(tableName);
    let result = await table.selectRecordsAsync();
    let missingFieldsByRecord = new Map();
    for (let record of result.records) {
        let missingFields = [];
        for (let fieldName of fieldNames) {
            if (!record.getCellValue(fieldName)) {
                missingFields.push(fieldName);
            }
        }
        if (missingFields.length > 0) {
            missingFieldsByRecord.set(record, missingFields);
        }
    }

    if (missingFieldsByRecord.size) {
        output.markdown(`### ｘ \`${missingFieldsByRecord.size}\` records in **${tableName}** are incomplete:`);
        missingFieldsByRecord.forEach((missingFields, record) => {
            let missingFieldsList = formatList(missingFields.map(name => `\`${name}\``));
            output.markdown(`- **${record.name}** is missing ${missingFieldsList}`);
        });
    } else {
        output.markdown(`### ✓ All records in **${tableName}** are complete`);
    }
    output.markdown('---');
}

await validateFields('Opportunities', ['Status']);
await validateFields('Interactions', ['Type']);
await validateFields('Accounts', ['HQ address']);
await validateFields('Contacts', ['Title', 'LinkedIn']);


Purchase_Order Block

let lineItemsTable = base.getTable('purchase_orders');
let furnitureTable = base.getTable('suppliers_catalog');


output.markdown('## Add a PO line item');

let furnitureRecord = await input.recordAsync('Pick a furniture item', furnitureTable);
if (furnitureRecord) {
    let quantity = parseInt(await input.textAsync('Enter the quantity'), 10);
    let recordId = await lineItemsTable.createRecordAsync({
        'furniture_items': [{ id: furnitureRecord.id }],
        'quantity': quantity
    });
        let allRecords = await lineItemsTable.selectRecordsAsync();
        let record = allRecords.records.find(record => record.id === recordId);
        output.markdown(`Added new line item: **${record.getCellValueAsString('Name')}**`);
    } else {
        output.markdown('No item picked');
}

Art_Placement Block

let lineItemsTable = base.getTable('art');
let inventoryTable = base.getTable('inventory');
let unitFurnitureTable = base.getTable('unit.furniture');

output.markdown('## How many art placements do you need?');
let placements = parseInt(await input.textAsync('Enter the quantity'), 10);

output.markdown('## Which unit is being leased?');
let leaseeRecord = await input.recordAsync('Select unit', unitFurnitureTable);

output.markdown('## Add an art item');
var inventoryRecord;
let recordId;
var artPlace;
var sqft;

for (var i = 1; i < placements+1; i++) {
    inventoryRecord = await input.recordAsync('Pick an item', inventoryTable);
    artPlace = 'placement.' + i;
    if (inventoryRecord) {
        if(i == 1) {
            // create record in art table
            recordId = await lineItemsTable.createRecordAsync({
                'inventory': [{ id: inventoryRecord.id }],
                'unit': [{ id: leaseeRecord.id }],
               // 'sq.ft.': leaseeRecord.getCellValue('sq.ft.'),
            });
    } else {
        // add additional art selections to the record created
        output.clear();
        var image = inventoryRecord.getCellValue('image');
        recordId = await lineItemsTable.updateRecordAsync(inventoryRecord, {
            'placement1': [
               // inventoryRecord.getCellValue('image'), {thumbnail: image} 
            ]
        });
    }
    let allRecords = await lineItemsTable.selectRecordsAsync();
    let record = allRecords.records.find(record => record.id === recordId);
    output.markdown(`Added new art item ${i} of ${placements}: **${record.getCellValueAsString('leasee')}** in table ${lineItemsTable.name}`);
        } else {
            output.markdown('No art picked');
    }
}